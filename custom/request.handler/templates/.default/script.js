function ajax_send(){
	$(document).on('click', '.submit-button', function(){
		var data = $(this).parents('form').serialize();
		$.ajax({
			type: 'POST',
			data: data,
			cache: false,
			success: function(responce){
				if(responce == '0'){
					alert('Спасибо за ваше обращение');
					$(document).find('form').reset();
				} else if(responce == '1'){
					alert('Не заполнено обязательное поле');
				} else if(responce == '2'){
					alert('Обнаружена попытка СПАМа');
					$(document).find('form').reset();
				}
			}
		});
		return false;
	});
}

$(document).on('ready', function(){
	ajax_send();
});