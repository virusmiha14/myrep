<?
use Bitrix\Main\Application;
use Bitrix\Main\Mail\Event;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

class requesthandler extends CBitrixComponent{
	
	protected $params;
	protected $responsecode;

	function initParams(){
		$this->params = Application::getInstance()->getContext()->getRequest();
		if($this->params->getPost('send')){
			$this->validateForm();
		}
	}
	
	private function validateForm(){
		
		if($this->params->getPost('email') && $this->params->getPost('title') && $this->params->getPost('text')){
			$this->responsecode = 0;
			self::createRequest();
		} else {
			$this->responsecode = 1;
			self::checkValidate();
		}
	}

	private function checkValidate(){
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
		echo $this->responsecode;
	}

	private function sendNotification(){
		Event::send(
			array(
				"EVENT_NAME" => "NEW_REQUEST",
				"LID" => "s1",
				"C_FIELDS" => array(
					"EMAIL_TO" => $this->arParams['NOTIFY_EMAIL'],
					"REQUEST_TITLE" => $this->params->getPost('title'),
					"REQUEST_TEXT" => $this->params->getPost('text'),
					"USER_EMAIL" => $this->params->getPost('email'),
				),
			)
		);
	}
	
	private function createRequest(){

		$el = new CIBlockElement;
		
		$arRequests = $el->GetList(
			array(),
			array(
				"IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
				"NAME" => $this->params->getPost('title'),
			)
		);

		if($arRequests->Fetch()){
			$this->responsecode = 2;
		} else {
			$arCreateRequestArray = array(
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
				"PROPERTY_VALUES" => array(),
				"NAME" => $this->params->getPost('title'),
				"ACTIVE" => "Y",
				"PREVIEW_TEXT" => $this->params->getPost('email'),
				"DETAIL_TEXT" => $this->params->getPost('text'),
			);

			$el->Add($arCreateRequestArray);
		}
			
		self::checkValidate();
	}
	
	public function executeComponent(){
		$this->initParams();
		if ($this->params->getPost('send') == 'true'){
			die();
		} else {
			$this->IncludeComponentTemplate();
		}
		//$this->IncludeComponentTemplate();
	}
}