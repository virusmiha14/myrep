<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	'NAME' => 'Форма создания обращения',
	'DESCRIPTION' => 'Компонент вывода формы создания обращения',
	'SORT' => 90,
	'CACHE_PATH' => 'Y',
	'PATH' => array(
		'ID' => 'custom',
		'NAME' => 'Пользовательские'
	),
);